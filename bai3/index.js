function demChanLe() {
  //   console.log("yes");
  var soThuNhat = document.getElementById("txt-so-thu-1").value * 1;
  var soThuHai = document.getElementById("txt-so-thu-2").value * 1;
  var soThuBa = document.getElementById("txt-so-thu-3").value * 1;

  var tongSoChan = 0;
  var tongSoLe = 0;

  if (soThuNhat % 2 == 0) {
    tongSoChan++;
  } else {
    tongSoLe++;
  }

  if (soThuHai % 2 == 0) {
    tongSoChan++;
  } else {
    tongSoLe++;
  }

  if (soThuBa % 2 == 0) {
    tongSoChan++;
  } else {
    tongSoLe++;
  }

  // console.log("số chẵn là: ", tongSoChan);
  // console.log("số lẻ là: ", tongSoLe);

  document.getElementById(
    "result"
  ).innerHTML = `Có ${tongSoChan} số chẵn, ${tongSoLe} số lẻ `;
}
