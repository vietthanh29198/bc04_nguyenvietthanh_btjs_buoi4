function chaoHoi() {
  var thanhVien = document.getElementById("txt-thanh-vien").value;

  switch (thanhVien) {
    case "B":
      document.getElementById(
        "result"
      ).innerHTML = `Xin chào <h1 class="text-danger">Bố</h1>`;
      break;
    case "M":
      document.getElementById(
        "result"
      ).innerHTML = `Xin chào <h1 class="text-success">Mẹ</h1>`;
      break;
    case "A":
      document.getElementById(
        "result"
      ).innerHTML = `Xin chào <h1 class="text-warning">Anh trai</h1>`;
      break;
    case "E":
      document.getElementById(
        "result"
      ).innerHTML = `Xin chào <h1 class="text-info">Em gái</h1>`;
      break;
    default:
      document.getElementById(
        "result"
      ).innerHTML = `HELLO <h1 class="text-secondary">Mấy cưng</h1>`;
  }
}
