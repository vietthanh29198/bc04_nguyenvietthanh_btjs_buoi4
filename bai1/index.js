function sapXep() {
  var soThu1 = document.getElementById("txt-so-thu-1").value * 1;
  var soThu2 = document.getElementById("txt-so-thu-2").value * 1;
  var soThu3 = document.getElementById("txt-so-thu-3").value * 1;

  if (soThu1 > soThu2 && soThu2 > soThu3) {
    result = `${soThu3} < ${soThu2} < ${soThu1}`;
  } else if (soThu1 > soThu2 && soThu3 > soThu2) {
    result = `${soThu2} < ${soThu3} < ${soThu1}`;
  } else if (soThu1 > soThu2 && soThu3 > soThu1) {
    result = `${soThu2} < ${soThu1} < ${soThu3}`;
  } else if (soThu2 > soThu1 && soThu1 > soThu3) {
    result = `${soThu3} < ${soThu1} < ${soThu2}`;
  } else if (soThu2 > soThu3 && soThu3 > soThu1) {
    result = `${soThu1} < ${soThu3} < ${soThu2}`;
  } else {
    result = `${soThu1} < ${soThu2} < ${soThu3}`;
  }

  document.getElementById(
    "result"
  ).innerHTML = `<h3 class="text-danger">${result}</h3>`;
}
