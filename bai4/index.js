function duDoan() {
  var doDaiCanh1 = document.getElementById("txt-do-dai-canh-1").value * 1;
  var doDaiCanh2 = document.getElementById("txt-do-dai-canh-2").value * 1;
  var doDaiCanh3 = document.getElementById("txt-do-dai-canh-3").value * 1;
  var result = 0;

  if (doDaiCanh1 == doDaiCanh2 && doDaiCanh2 == doDaiCanh3) {
    result = "Hình Tam Giác Đều";
  } else if (
    doDaiCanh1 == doDaiCanh2 ||
    doDaiCanh2 == doDaiCanh3 ||
    doDaiCanh1 == doDaiCanh3
  ) {
    result = "Hình Tam Giác Cân";
  } else if (
    doDaiCanh1 * doDaiCanh1 ==
      doDaiCanh2 * doDaiCanh2 + doDaiCanh3 * doDaiCanh3 ||
    doDaiCanh2 * doDaiCanh2 ==
      doDaiCanh1 * doDaiCanh1 + doDaiCanh3 * doDaiCanh3 ||
    doDaiCanh3 * doDaiCanh3 == doDaiCanh1 * doDaiCanh1 + doDaiCanh2 * doDaiCanh2
  ) {
    result = "Hình Tam Giác Vuông";
  } else {
    result = "Một Hình Tam Giác Nào Đó";
  }

  document.getElementById("result").innerHTML = `${result}`;
}
